import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GitHubServiceProvider } from '../../providers/git-hub-service/git-hub-service';
import { User } from '../../models/user.interface';
import { Repository } from '../../models/repository.interface';

/**
 * Generated class for the ProfileSearchResultPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage({
  segment: 'profile/results/:username'
})
@Component({
  selector: 'page-profile-search-result',
  templateUrl: 'profile-search-result.html',
})
export class ProfileSearchResultPage {
  username: string = '';
  user: User;
  repositories: Repository[];

  constructor(private navCtrl: NavController, private navParams: NavParams, private gitHubService: GitHubServiceProvider) {
  }

  getUserInformation(): void {
    this.gitHubService.getUserInformation(this.username).subscribe((user: User) => this.user = user);
    this.gitHubService.getRepositoryInformation(this.username).subscribe((repositories: Repository[]) => this.repositories = repositories);
    // if (this.username) {
    //   this.gitHubService.mockGetUserInformation(this.username).subscribe(
    //     (user: User) => this.user = user,
    //     (error) => console.log(error)
    //   );
    //   this.gitHubService.mockGetUserRepositories(this.username).subscribe(
    //     (repositories: Repository[]) => this.repositories = repositories,
    //     (error) => console.log(error)
    //   )
    // }
  }

  goBackToSearchPage(): void {
    this.navCtrl.pop();
  }

  ionViewWillLoad() {
    this.username = this.navParams.get('username');
    this.getUserInformation();
  }
}
