import { Repository } from '../models/repository.interface';
import { USER_LIST } from './user.mocks';

const repositoryList: Repository[] = [
  {
    name: 'Some name1',
    description: 'Some ver as das das d ad asd adasdasd',
    owner: USER_LIST[0]
  },
  {
    name: 'Some name2',
    description: 'Some ver as das das d ad asd adasdasd',
    owner: USER_LIST[0]
  },
  {
    name: 'Some name3',
    description: 'Some ver as das das d ad asd adasdasd',
    owner: USER_LIST[0]
  },
  {
    name: 'Some name4',
    description: 'Some ver as das das d ad asd adasdasd',
    owner: USER_LIST[1]
  },
  {
    name: 'Some name5',
    description: 'Some ver as das das d ad asd adasdasd',
    owner: USER_LIST[1]
  }
];

export const REPOSITORY_LIST = repositoryList;