import { User } from '../models/user.interface';

const userList: User[] = [
  {
    name: 'Michał Kungonda',
    company: 'RTC',
    location: 'Kosakowo, Gdynia',
    bio: 'Having fun with a lot of thins!',
    avatar_url: 'http://www.sportcom.qc.ca/wp-content/uploads/sports-icons/sports-tir-paralympique.png',
    email: 'mkungonda@gmail.com'
  },
  {
    name: 'Piotr Obobok',
    company: 'RTC',
    location: 'Kosakowo, Gdynia',
    bio: 'Having fun with a lot of thins!',
    avatar_url: 'http://www.sportcom.qc.ca/wp-content/uploads/sports-icons/sports-tir-paralympique.png',
    email: 'piotr@gmail.com'
  },
]

export const USER_LIST = userList;